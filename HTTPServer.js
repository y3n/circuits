const http = require("http");
const fs = require("fs");
const fsP = require("fs").promises;

const Assets = require("./Assets");
const ArduinoCompiler = require("./ArduinoCompiler");
const Editor = require("./Editor");
const Channel = require("./Channel");

module.exports = class HTTPServer {
  constructor(n) {
    this.n = n;

    this.staticFiles = [
      // JS FILES
      { path: "circuits-compiled.js", type: "text/javascript" },
      { path: "v1.js", type: "text/javascript" },
      { path: "vendor-compiled.js", type: "text/javascript" },
      { path: "dashboard.js", type: "text/javascript" },
      // CSS FILES
      { path: "circuits.css", type: "text/css" },
      { path: "font-awesome.min.css", type: "text/css" },
      { path: "fonts.css", type: "text/css" },
      { path: "dashboard.css", type: "text/css" },
      // FONTS FILES
      { path: "Roboto/Roboto-Regular.woff2", type: "font/woff2" },
      { path: "Roboto/Roboto-Regular-special.woff2", type: "font/woff2" },
      { path: "Roboto/Roboto-Medium.woff2", type: "font/woff2" },
      // IMG FILES
      { path: "favicon.ico", type: "image/vnd.microsoft.icon" },
      { path: "avatar.png", type: "image/png" }
    ];

    this.config = n.config;
    this.log = n.log;
    this.db = n.db;

    this.assets = new Assets(this.n);
    this.compiler = new ArduinoCompiler(this.n);
    this.editor = new Editor(this.n, this);
    this.channel = new Channel(this.n, this);
  }

  run() {
    this.assets.init();
    this.compiler.init();
    this.editor.init();
    this.channel.init();

    this.http = http.createServer((req, res) => {
      req.ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
      this.route(req, res);
    });
    this.http.listen(this.config.port, this.config.host);
    this.log.info(`HTTP server started on ${this.config.host}:${this.config.port}`);
  }

  async route(req, res) {
    this.log.info(`${req.ip} ${req.method} ${req.url}`);

    req.user = await this.getUser(req, res);

    if (req.method == "GET" && req.url == "/") {
      this.sendStaticHTML(req, res, "dashboard.html");
      return;
    }

    if (req.method == "GET" && req.url == "/create") {
      this.editor.create(req, res);
      return;
    }

    if (req.method == "GET" && req.url == "/circuits") {
      this.editor.handleCircuits(req, res);
      return;
    }

    if (req.method == "POST" && req.url == "/compile") {
      this.handleCompile(req, res);
      return;
    }

    if (req.method == "POST" && req.url == "/import") {
      this.editor.import(req, res);
      return;
    }

    if (req.method == "POST" && req.url == "/account") {
      this.handleAccount(req, res);
      return;
    }

    if (req.method == "GET" && req.url.startsWith("/proxy/")) {
      this.handleAssetProxy(req, res);
      return;
    }

    if (req.method == "POST" && req.url.startsWith("/tinkercad/thumbupload")) {
      this.editor.handleThumbUpload(req, res);
      return;
    }

    let match = req.url.match(/\/thumbs\/([A-z0-9]+).png/);
    if (req.method == "GET" && match != null) {
      //this.sendStaticHTML(req, res, "editel.html", match[1]);
      this.editor.handleThumb(req, res, match[1]);
      return;
    }

    match = req.url.match(/\/c\/([A-z0-9]+)/);
    if (match != null) {
      //this.sendStaticHTML(req, res, "editel.html", match[1]);
      this.editor.handle(req, res, match[1]);
      return;
    }
    match = req.url.match(/\/dashboard\/designs\/([A-z0-9]+)/);
    if (req.method == "PATCH" && match != null) {
      //this.sendStaticHTML(req, res, "editel.html", match[1]);
      this.editor.patch(req, res, match[1]);
      return;
    }

    // /channel/test?a=circuits%7C67ec30dea422765ace41fc020b2d85ae%7Ctinkerauth&VER=8&MODE=init&zx=56cir2cdlwzp&t=1
    match = req.url.match(/^\/channel\//);
    if (match != null) {
      this.channel.handle(req, res);
      return;
    }

    if (req.method == "GET") {
      for (let file of this.staticFiles) {
        if (req.url == "/" + file.path) {
          this.sendStaticFile(req, res, file);
          return;
        }
      }
    }

    res.writeHead(404);
    res.end();
  }

  async getUser(req, res) {
    if(!req.headers.cookie) {
      return null;
    }

    const match = req.headers.cookie.match(/token=([A-z0-9]+)/);
    if(!match) return null;
    
    let tokenStr = match[1];
    let token = await this.db.collection("tokens").findOne({ token: tokenStr });
    req.token = token;
    if(!token) return null;
    return await this.db.collection("accounts").findOne({ id: token.user });
  }

  async sendStaticHTML(req, res, filePath) {
    let content = await fsP.readFile("client/" + filePath, { encoding: "utf8" });
    res.setHeader("content-type", "text/html; charset=utf-8");
    res.setHeader("content-length", Buffer.byteLength(content, 'utf8'));
    res.end(content);
  }

  sendStaticFile(req, res, file) { // binary-level
    res.setHeader("content-type", file.type);
    res.setHeader("cache-control", "public, max-age=604800, immutable");

    const stream = fs.createReadStream("client/" + file.path, { emitClose: true })
    stream.pipe(res);
  }

  async handleCompile(req, res) {
    let body = await this.getBody(req);
    body = JSON.parse(body);
    //console.log(`compile: ${body}`);
    const promises = [];

    for (let build of body) {
      promises.push(this.buildSketch(build));
    }
    res.end(JSON.stringify(await Promise.all(promises)));
  }

  async handleAccount(req, res) {
    let body = await this.getBody(req);
    body = JSON.parse(body);

    let account;
    if(body.key) {
      account = await this.db.collection("accounts").findOne({ key: body.key });
      if(!account) {
        res.writeHead(404);
        res.end();
        return;
      }
    } else {
      account = {
        id: this.n.randomString.create(),
        creationDate: new Date(),
        username: body.username,
        email: body.email,
        key: this.n.randomString.create(32)
      }
      await this.db.collection("accounts").insertOne(Object.assign({}, account));
    }
    account.token = await this.createToken(req, account);
    res.setHeader("set-cookie", "token=" + account.token);

    let resBody = JSON.stringify(account);
    res.setHeader("content-type", "application/json");
    res.setHeader("content-length", Buffer.byteLength(resBody, 'utf8'));
    res.end(resBody);
  }

  async createToken(req, account) {
    let token = {
      user: account.id,
      creationDate: new Date(),
      ip: req.ip,
      token: this.n.randomString.create(32)
    }

    await this.db.collection("tokens").insertOne(token);

    return token.token;
  }

  async buildSketch(build) {
    let compilerOutput = await this.compiler.compile(build.code["sketch.ino"], `${build.type}_${build.variant}`);
    let result = {
      compiler_guid: build.compiler_guid,
      state: compilerOutput.state
    }

    if (compilerOutput.state == "success") {
      result.elf = JSON.stringify({
        hex: compilerOutput.hex,
        dwarf: {}
      });

      result.url = "wonderland";
    } else {
      result.stderr = compilerOutput.stderr;
    }
    return result;
  }

  async handleAssetProxy(req, res) {
    const file = await this.assets.get(req.url.substring(6));
    res.setHeader("content-type", file.type);
    res.setHeader("cache-control", "public, max-age=604800, immutable");
    //res.setHeader("content-length", file.length);
    file.stream.pipe(res);
  }

  getBody(req) {
    const chunks = [];
    return new Promise((r) => {
      req.on("data", (data) => {
        chunks.push(data);
      });

      req.on("end", () => {
        r(Buffer.concat(chunks));
      })
    })
  }
}