const fsP = require("fs").promises;
const { spawn } = require('child_process');

const RandomString = require("./RandomString");

module.exports = class ArduinoCompiler {
  constructor(n) {
    this.n = n;

    this.idGen = new RandomString(this);
    this.fqbns = {
      "arduino_uno": "arduino:avr:uno",
      "arduino_micro": "arduino:avr:micro"
    }
  }

  init() {

  }

  async compile(sketch, platform) {
    const fqbn = this.fqbns[platform];
    const buildID = this.idGen.create();
    await fsP.mkdir(`builds/${buildID}`);
    await fsP.writeFile(`builds/${buildID}/${buildID}.ino`, sketch);

    let [code, stderr] = await this.startProcess(buildID, fqbn);

    if(code !== 0) {
      return {
        state: "error",
        stderr: stderr
      }
    }
    let hexPath = `builds/${buildID}/${buildID}.${fqbn.replace(/:/g, ".")}.hex`;
    let hex = await fsP.readFile(hexPath, { encoding: "utf8" });

    // cleaning
    fsP.rmdir(`builds/${buildID}`, { recursive: true });

    return {
      state: "success",
      hex: hex
    }
  }

  startProcess(buildID, fqbn) {
    const run = spawn('arduino-cli', ['compile', '--fqbn', fqbn, `builds/${buildID}`]);
    let stderr = ``;

    run.stdout.on('data', (data) => {
      //console.log(data.toString());
    });

    run.stderr.on('data', (data) => {
      //console.error("stderr: " + data.toString());
      stderr += data.toString().replace(/([^ \n]+)\.ino:/g, "");
      //console.error("stderr (parsed): " + stderr);
    });

    return new Promise((r) => {
      run.on('exit', (code) => {
        r([code, stderr]);
      })
    })
  }
}