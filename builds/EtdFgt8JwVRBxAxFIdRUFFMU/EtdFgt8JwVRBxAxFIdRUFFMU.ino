#include <Wire.h>

//LiquidTWI2.h:
#include <inttypes.h>
#include "Print.h"

// for memory-constrained projects, comment out the MCP230xx that doesn't apply
#define MCP23008 // Adafruit I2C Backpack

// if DETECT_DEVICE is enabled, then when constructor's detectDevice != 0
// device will be detected in the begin() function...
// if the device isn't detected in begin() then we won't try to talk to the
// device in any of the other functions... this allows you to compile the
// code w/o an LCD installed, and not get hung in the write functions
#define DETECT_DEVICE // enable device detection code

#define MCP23008_ADDRESS 0x20

// registers
#define MCP23008_IODIR 0x00
#define MCP23008_IPOL 0x01
#define MCP23008_GPINTEN 0x02
#define MCP23008_DEFVAL 0x03
#define MCP23008_INTCON 0x04
#define MCP23008_IOCON 0x05
#define MCP23008_GPPU 0x06
#define MCP23008_INTF 0x07
#define MCP23008_INTCAP 0x08
#define MCP23008_GPIO 0x09
#define MCP23008_OLAT 0x0A

// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_BACKLIGHT 0x08 // used to pick out the backlight flag since _displaycontrol will never set itself

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
//we only support 4-bit mode #define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00

class LiquidTWI2 : public Print {
public:
LiquidTWI2(uint8_t i2cAddr,uint8_t detectDevice=0,uint8_t backlightInverted=0);

void begin(uint8_t cols, uint8_t rows,uint8_t charsize = LCD_5x8DOTS);

#ifdef DETECT_DEVICE
uint8_t LcdDetected() { return _deviceDetected; }
#endif // DETECT_DEVICE
void clear();
void home();

void noDisplay();
void display();
void noBlink();
void blink();
void noCursor();
void cursor();
void scrollDisplayLeft();
void scrollDisplayRight();
void leftToRight();
void rightToLeft();
void autoscroll();
void noAutoscroll();

void setBacklight(uint8_t status);

void createChar(uint8_t, uint8_t[]);
void setCursor(uint8_t, uint8_t);
#if defined(ARDUINO) && (ARDUINO >= 100) // scl
virtual size_t write(uint8_t);
#else
virtual void write(uint8_t);
#endif
void command(uint8_t);

private:
void send(uint8_t, uint8_t);
void burstBits8(uint8_t);

uint8_t _displayfunction;
uint8_t _displaycontrol;
uint8_t _displaymode;
uint8_t _numlines,_currline;
uint8_t _i2cAddr;
uint8_t _backlightInverted;
#ifdef DETECT_DEVICE
uint8_t _deviceDetected;
#endif // DETECT_DEVICE

};



//====================================================
//LiquidTWI2.cpp: 


/*
  LiquidTWI2 High Performance i2c LCD driver for MCP23008 & MCP23017
  hacked by Sam C. Lin / http://www.lincomatic.com
  from 
   LiquidTWI by Matt Falcon (FalconFour) / http://falconfour.com
   logic gleaned from Adafruit RGB LCD Shield library
   Panelolu2 support by Tony Lock / http://blog.think3dprint3d.com
   enhancements by Nick Sayer / https://github.com/nsayer

  Compatible with Adafruit I2C LCD backpack (MCP23008) and
  Adafruit RGB LCD Shield
*/

#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <Wire.h>
#if defined(ARDUINO) && (ARDUINO >= 100) //scl
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

static inline void wiresend(uint8_t x) {
#if ARDUINO >= 100
  Wire.write((uint8_t)x);
#else
  Wire.send(x);
#endif
}

// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set: 
//    DL = 0; 4-bit interface data 
//    N = 0; 1-line display 
//    F = 0; 5x8 dot character font 
// 3. Display on/off control: 
//    D = 0; Display off 
//    C = 0; Cursor off 
//    B = 0; Blinking off 
// 4. Entry mode set: 
//    I/D = 1; Increment by 1 
//    S = 0; No shift 
//
// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that its in that state when a sketch starts (and the
// LiquidTWI2 constructor is called). This is why we save the init commands
// for when the sketch calls begin(), except configuring the expander, which
// is required by any setup.

LiquidTWI2::LiquidTWI2(uint8_t i2cAddr,uint8_t detectDevice, uint8_t backlightInverted) {
  // if detectDevice != 0, set _deviceDetected to 2 to flag that we should
  // scan for it in begin()
#ifdef DETECT_DEVICE
  _deviceDetected = detectDevice ? 2 : 1;
#endif

  _backlightInverted = backlightInverted;

  //  if (i2cAddr > 7) i2cAddr = 7;
  _i2cAddr = i2cAddr; // transfer this function call's number into our internal class state
  _displayfunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS; // in case they forget to call begin() at least we have something
}

void LiquidTWI2::begin(uint8_t cols, uint8_t lines, uint8_t dotsize) {
  // SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
  // according to datasheet, we need at least 40ms after power rises above 2.7V
  // before sending commands. Arduino can turn on way befer 4.5V so we'll wait 50
  delay(50);

  Wire.begin();

  uint8_t result;

	  
    // now we set the GPIO expander's I/O direction to output since it's soldered to an LCD output.
    Wire.beginTransmission(MCP23008_ADDRESS | _i2cAddr);
    wiresend(MCP23008_IODIR);
    wiresend(0x00); // all output: 00000000 for pins 1...8
    result = Wire.endTransmission();
#ifdef DETECT_DEVICE
    if (result) {
        if (_deviceDetected == 2) {
          _deviceDetected = 0;
          return;
        }
    }
#endif 

#ifdef DETECT_DEVICE
  // If we haven't failed by now, then we pass
  if (_deviceDetected == 2) _deviceDetected = 1;
#endif

  if (lines > 1) {
    _displayfunction |= LCD_2LINE;
  }
  _numlines = lines;
  _currline = 0;

  //put the LCD into 4 bit mode
  // start with a non-standard command to make it realize we're speaking 4-bit here
  // per LCD datasheet, first command is a single 4-bit burst, 0011.
  //-----
  //  we cannot assume that the LCD panel is powered at the same time as
  //  the arduino, so we have to perform a software reset as per page 45
  //  of the HD44780 datasheet - (kch)
  //-----
    // bit pattern for the burstBits function is
    //
    //  7   6   5   4   3   2   1   0
    // LT  D7  D6  D5  D4  EN  RS  n/c
    //-----
    burstBits8(B10011100); // send LITE D4 D5 high with enable
    burstBits8(B10011000); // send LITE D4 D5 high with !enable
    burstBits8(B10011100); //
    burstBits8(B10011000); //
    burstBits8(B10011100); // repeat twice more
    burstBits8(B10011000); //
    burstBits8(B10010100); // send D4 low and LITE D5 high with enable
    burstBits8(B10010000); // send D4 low and LITE D5 high with !enable

  delay(5); // this shouldn't be necessary, but sometimes 16MHz is stupid-fast.

  command(LCD_FUNCTIONSET | _displayfunction); // then send 0010NF00 (N=lines, F=font)
  delay(5); // for safe keeping...
  command(LCD_FUNCTIONSET | _displayfunction); // ... twice.
  delay(5); // done!

  // turn on the LCD with our defaults. since these libs seem to use personal preference, I like a cursor.
  _displaycontrol = (LCD_DISPLAYON|LCD_BACKLIGHT);
  display();
  // clear it off
  clear();

  _displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
  // set the entry mode
  command(LCD_ENTRYMODESET | _displaymode);
}

/********** high level commands, for the user! */
void LiquidTWI2::clear()
{
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  command(LCD_CLEARDISPLAY);  // clear display, set cursor position to zero
  delayMicroseconds(2000);  // this command takes a long time!
}

void LiquidTWI2::home()
{
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  command(LCD_RETURNHOME);  // set cursor position to zero
  delayMicroseconds(2000);  // this command takes a long time!
}

void LiquidTWI2::setCursor(uint8_t col, uint8_t row)
{
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
  if ( row > _numlines ) row = _numlines - 1;    // we count rows starting w/0
  command(LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

// Turn the display on/off (quickly)
void LiquidTWI2::noDisplay() {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  _displaycontrol &= ~LCD_DISPLAYON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void LiquidTWI2::display() {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  _displaycontrol |= LCD_DISPLAYON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turns the underline cursor on/off
void LiquidTWI2::noCursor() {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  _displaycontrol &= ~LCD_CURSORON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void LiquidTWI2::cursor() {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  _displaycontrol |= LCD_CURSORON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turn on and off the blinking cursor
void LiquidTWI2::noBlink() {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  _displaycontrol &= ~LCD_BLINKON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void LiquidTWI2::blink() {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  _displaycontrol |= LCD_BLINKON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// These commands scroll the display without changing the RAM
void LiquidTWI2::scrollDisplayLeft(void) {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}
void LiquidTWI2::scrollDisplayRight(void) {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void LiquidTWI2::leftToRight(void) {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  _displaymode |= LCD_ENTRYLEFT;
  command(LCD_ENTRYMODESET | _displaymode);
}

// This is for text that flows Right to Left
void LiquidTWI2::rightToLeft(void) {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  _displaymode &= ~LCD_ENTRYLEFT;
  command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'right justify' text from the cursor
void LiquidTWI2::autoscroll(void) {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  _displaymode |= LCD_ENTRYSHIFTINCREMENT;
  command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'left justify' text from the cursor
void LiquidTWI2::noAutoscroll(void) {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  _displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
  command(LCD_ENTRYMODESET | _displaymode);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void LiquidTWI2::createChar(uint8_t location, uint8_t charmap[]) {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  location &= 0x7; // we only have 8 locations 0-7
  command(LCD_SETCGRAMADDR | (location << 3));
  for (int i=0; i<8; i++) {
    write(charmap[i]);
  }
}

/*********** mid level commands, for sending data/cmds */
inline void LiquidTWI2::command(uint8_t value) {
  send(value, LOW);
}
#if defined(ARDUINO) && (ARDUINO >= 100) //scl
inline size_t LiquidTWI2::write(uint8_t value) {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return 1;
#endif
  send(value, HIGH);
  return 1;
}
#else
inline void LiquidTWI2::write(uint8_t value) {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  send(value, HIGH);
}
#endif

/************ low level data pushing commands **********/

// Allows to set the backlight, if the LCD backpack is used
void LiquidTWI2::setBacklight(uint8_t status) {
#ifdef DETECT_DEVICE
  if (!_deviceDetected) return;
#endif
  if (_backlightInverted) status ^= 0x7;
    bitWrite(_displaycontrol,3,status); // flag that the backlight is enabled, for burst commands
    burstBits8((_displaycontrol & LCD_BACKLIGHT)?0x80:0x00);
}

// write either command or data, burst it to the expander over I2C.
void LiquidTWI2::send(uint8_t value, uint8_t mode) {
    // BURST SPEED, OH MY GOD
    // the (now High Speed!) I/O expander pinout
    // RS pin = 1
    // Enable pin = 2
    // Data pin 4 = 3
    // Data pin 5 = 4
    // Data pin 6 = 5
    // Data pin 7 = 6
    byte buf;
    // crunch the high 4 bits
    buf = (value & B11110000) >> 1; // isolate high 4 bits, shift over to data pins (bits 6-3: x1111xxx)
    if (mode) buf |= 3 << 1; // here we can just enable enable, since the value is immediately written to the pins
    else buf |= 2 << 1; // if RS (mode), turn RS and enable on. otherwise, just enable. (bits 2-1: xxxxx11x)
    buf |= (_displaycontrol & LCD_BACKLIGHT)?0x80:0x00; // using DISPLAYCONTROL command to mask backlight bit in _displaycontrol
    burstBits8(buf); // bits are now present at LCD with enable active in the same write
    // no need to delay since these things take WAY, WAY longer than the time required for enable to settle (1us in LCD implementation?)
    buf &= ~(1<<2); // toggle enable low
    burstBits8(buf); // send out the same bits but with enable low now; LCD crunches these 4 bits.
    // crunch the low 4 bits
    buf = (value & B1111) << 3; // isolate low 4 bits, shift over to data pins (bits 6-3: x1111xxx)
    if (mode) buf |= 3 << 1; // here we can just enable enable, since the value is immediately written to the pins
    else buf |= 2 << 1; // if RS (mode), turn RS and enable on. otherwise, just enable. (bits 2-1: xxxxx11x)
    buf |= (_displaycontrol & LCD_BACKLIGHT)?0x80:0x00; // using DISPLAYCONTROL command to mask backlight bit in _displaycontrol
    burstBits8(buf);
    buf &= ~( 1 << 2 ); // toggle enable low (1<<2 = 00000100; NOT = 11111011; with "and", this turns off only that one bit)
    burstBits8(buf);
}

void LiquidTWI2::burstBits8(uint8_t value) {
  // we use this to burst bits to the GPIO chip whenever we need to. avoids repetitive code.
  Wire.beginTransmission(MCP23008_ADDRESS | _i2cAddr);
  wiresend(MCP23008_GPIO);
  wiresend(value); // last bits are crunched, we're done.
  while(Wire.endTransmission());
}

// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// ############################################################
// USER CODE

// SENSORS PINS
#define trigR 9
#define echoR 10
#define trigL 11
#define echoL 12
#define IR 0

// REMOTE PINS
#define remoteMode 11
#define remoteDir 12
#define remoteSpd 13

// MOTORS PINS
/*#define ENA 3
  #define IN1 4
  #define IN2 5
  #define ENB 6
  #define IN3 7
  #define IN4 8*/
#define ENA 6
#define IN1 8
#define IN2 7
#define ENB 3
#define IN3 4
#define IN4 5

// DEBUG
#define DEBUG_TC // tinkercad
#define DEBUG_LCD
//#define DEBUG_SERIAL

// INTERVAL
#define intervalUS 12
#define intervalIR 20
#define intervalCompass 1000

// DIRS
#define RIGHT 1
#define LEFT 0
#define FORW 1
#define BACKW -1
#define STOP 0

#ifndef DEBUG_TC
#include <LiquidCrystal_I2C.h>
#endif

#include <Wire.h>

#ifdef DEBUG_TC
LiquidTWI2 lcd(0x20);
#else
LiquidCrystal_I2C lcd(0x27, 16, 2);
#endif

bool us = RIGHT; // current US
byte unlock = 0;
int distUS[2], distIR, leftPow, rightPow, compass, oldCompass, leftDir, rightDir;
unsigned long time, previousUS = 0, previousIR = 0, previousCompass = 0;
float powComp, powComp2;

bool modeAuto = false;

void setup() {
  analogReference(DEFAULT);
  pinMode(A0, INPUT);

#ifdef DEBUG_SERIAL
  Serial.begin(9600);
#endif

#ifdef DEBUG_LCD
#ifdef DEBUG_TC
  lcd.begin(16, 2);
#else
  lcd.begin();
  lcd.backlight();
#endif
#endif

  // US
  pinMode(trigR, OUTPUT);
  pinMode(echoR, INPUT);
  pinMode(trigL, OUTPUT);
  pinMode(echoL, INPUT);

  digitalWrite(trigR, LOW);
  digitalWrite(trigL, LOW);

  // IR
  analogReference(DEFAULT);
}

void loop() {
  time = millis();
  
  if (pulseIn(remoteMode, HIGH, 22e3) < 1500) {
    // MANUAL
    
    // powComp & powComp2 = float
    powComp = 0.28284371; // sqrt(2)/5
    powComp *= pulseIn(remoteSpd, HIGH, 30e3)-1500;
    powComp2 = (PI/1e3)*(1750-pulseIn(remoteDir, HIGH, 30e3));
    
    // compute left pow
    leftPow = powComp*cos(powComp2);
    if(leftPow < 0) {
      leftDir = BACKW;
      leftPow *= -1;
    } else {
      leftDir = FORW;
    }
    if(leftPow > 100) leftPow = 100;
    
    // compute right pow
    rightPow = powComp*sin(powComp2);
    if(rightPow < 0) {
      rightDir = BACKW;
      rightPow *= -1;
    } else {
      rightDir = FORW;
    }
    if(rightPow > 100) rightPow = 100;
    
#ifdef DEBUG_LCD
    lcd.setCursor(0, 1);
    lcd.print(leftPow * leftDir);
    lcd.print("  ");
    lcd.setCursor(7, 1);
    lcd.print("MANU      ");
    lcd.setCursor(13, 1);
    lcd.print(rightPow * rightDir);
#endif
    
    // MOTORS CONTROL
    cmd_motor(LEFT, leftDir, leftPow);
    cmd_motor(RIGHT, rightDir, rightPow);
  } else {
    // AUTONOMOUS
    // GET MEASURES
    // - US
    if ((time - previousUS) > intervalUS) {
      us = !us;
      distUS[us] = distanceUS(us);

#ifdef DEBUG_LCD
      lcd.setCursor(13 * us, 0);
      lcd.print(distUS[us], DEC);
      lcd.print("  ");
#endif

#ifdef DEBUG_SERIAL
      Serial.print("US L:");
      Serial.print(distUS[LEFT]);
      Serial.print(" cm  R:");
      Serial.print(distUS[RIGHT]);
      Serial.println(" cm");
#endif

      previousUS = time;
    }

    // - IR
    if ((time - previousIR) > intervalIR) {
      distIR = distanceIR();
#ifdef DEBUG_LCD
      lcd.setCursor(7, 0);
      lcd.print(distIR, DEC);
      lcd.print("  ");
#endif

#ifdef DEBUG_SERIAL
      Serial.print("IR: ");
      Serial.print(distIR);
      Serial.println(" cm");
#endif
      previousIR = time;
    }

    // - COMPASS
    if ((time - previousCompass) > intervalCompass) {
      compass = acquireCompass(50);
      unlock = abs(oldCompass - compass) < 2 ? 1 : 0;
      oldCompass = compass;
      previousCompass = time;
    }

    // MOTORS CONTROL
    if (unlock != 0) {
      leftDir = BACKW;
      rightDir = BACKW;
      unlock = random(0, 3);

      switch (unlock) {
        case 0: // LEFT
          leftPow = 40;
          rightPow = 60;
          break;
        case 1: // RIGHT
          leftPow = 60;
          rightPow = 40;
          break;
        default: // STRAIGHT BACK
          leftPow = 60;
          rightPow = 60;
      }
    } else {
      if (distIR < 15) {
        // ArrÃªt des deux moteurs
        leftDir = STOP;
        rightDir = STOP;
        leftPow = 0;
        rightPow = 0;
#ifdef DEBUG_LCD
        lcd.setCursor(7, 1);
        lcd.print("STOP");
#endif
      } else if (distIR >= 15 && distIR <= 25) {
        // Le robot tourne du cÃ´tÃ© ou il y a le plus de place,
        // un moteur arrÃªtÃ© lâ€™autre Ã  60 %
        if (distUS[LEFT] < distUS[RIGHT]) {
          leftDir = FORW;
          rightDir = STOP;
          leftPow = 30;
          rightPow = 0;
#ifdef DEBUG_LCD
          lcd.setCursor(7, 1);
          lcd.print("FORW ");
#endif
        } else {
          leftDir = STOP;
          rightDir = FORW;
          leftPow = 0;
          rightPow = 30;
#ifdef DEBUG_LCD
          lcd.setCursor(7, 1);
          lcd.print("BACKW ");
#endif
        }
      } else {
        // La roue droite est asservie Ã  la distance US gauche
        // (entre 20 et 60%) et la roue gauche est asservie Ã  la
        // distance US droite (entre 20 et 60%).
        leftDir = FORW;
        rightDir = FORW;
        leftPow = 20 + 40 * (distUS[RIGHT] / 100.0);
        rightPow = 20 + 40 * (distUS[LEFT] / 100.0);
      }
    }
    
    // motor commands
    cmd_motor(LEFT, leftDir, leftPow);
    cmd_motor(RIGHT, rightDir, rightPow);

#ifdef DEBUG_LCD
    lcd.setCursor(0, 1);
    lcd.print(leftPow * leftDir);
    lcd.print("  ");
    lcd.setCursor(7, 1);
    lcd.print(unlock != 0 ? "BACKW" : "FORW ");
    lcd.setCursor(13, 1);
    lcd.print(rightPow * rightDir);
    lcd.print("  ");
#endif

    if (unlock != 0) {
      delay(1000);
    }
  }
}

int distanceUS(bool side) {
  int t;
  if (side == RIGHT) {
    digitalWrite(trigR, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigR, LOW);
    t = pulseIn(echoR, HIGH, 5882);
  } else {
    digitalWrite(trigL, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigL, LOW);
    t = pulseIn(echoL, HIGH, 5882);
  }
  t *= 0.0172;
  if (t == 0) t = 100;
  return t;
}

int distanceIR(void) {
  return 19617 * pow(analogRead(IR), -1.232);
}

void cmd_motor(int motor, int dir, int power) {
  analogWrite(
    motor == LEFT ? ENA : ENB,
    dir != STOP ? power * 2.55 : 0
  );
  digitalWrite(
    motor == LEFT ? IN1 : IN3,
    dir == FORW ? HIGH : LOW
  );
  digitalWrite(
    motor == LEFT ? IN2 : IN4,
    dir == BACKW ? HIGH : LOW
  );
}

int acquireCompass(int trend) {
  int val = random(trend - 10, trend + 10);
  if (val < 0) val += 360;
  else if (val > 360) val -= 360
  return val;
}