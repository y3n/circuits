const HTTPServer = require("./HTTPServer");
const Logger = require("./Logger");
const Database = require("./Database");
const RandomString = require("./RandomString");

module.exports = class TCServer {
  constructor(config) {
    this.config = config;
    this.log = new Logger(this.config);
  }

  async run() {
    this.randomString = new RandomString();

    this.dbManager = new Database(this);
    this.db = await this.dbManager.connect();

    this.server = new HTTPServer(this);
    this.server.run();
  }
}