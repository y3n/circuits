const fs = require("fs");
const fsP = require("fs").promises;
const https = require("https");
const path = require("path");

module.exports = class Assets {
  constructor(n) {
    this.n = n;
    this.log = n.log;
    this.config = n.config;
  }

  async init() {
    try {
      this.index = JSON.parse(await fsP.readFile("assets/index.json", { encoding: "utf-8" }));
    } catch(e) {
      console.error(e);
      this.index = {};
    }

    this.log.info(`[assets] loaded ${Object.keys(this.index).length} cached assets`);
  }

  async get(path) {
    let file = this.index[path];
    if (file) {
      return {
        type: file.type,
        length: file.length,
        stream: fs.createReadStream(`assets${path}`, { emitClose: true })
      }
    } else {
      const file = await this.getRemoteFile(path);
      await this.store(path, file);
      //console.log(path, file);
      file.stream = fs.createReadStream(`assets${path}`);
      //console.log(file.stream);
      return file;
    }
  }

  async store(path, file) {
    const copy = Object.assign({}, file);
    // save file
    await this.writeFile(path, copy.stream);
    // save index
    delete copy.stream;
    this.index[path] = copy;
    await fsP.writeFile(`assets/index.json`, JSON.stringify(this.index));
  }

  writeFile(path, stream) {
    return new Promise((r) => {
      const filePath = `assets${path}`;
      this.ensureDirectoryExistence(filePath);
      const ws = fs.createWriteStream(filePath);
      const wsP = stream.pipe(ws);
      ws.on("finish", () => {
        //console.log(ws);
        r();
      });
    });
  }

  getRemoteFile(path) {
    const url = `https://beta-editor.tinkercad.com${path}`;
    //console.log(`proxied url: ${url}`);

    return new Promise((r) => {
      https.get(url, (res) => {
        r({
          type: res.headers["content-type"],
          length: res.headers["content-length"],
          stream: res
        });
      });
    })
  }

  async ensureDirectoryExistence(filePath) {
    var dirname = path.dirname(filePath);
    if (fs.existsSync(dirname)) {
      return true;
    }
    this.ensureDirectoryExistence(dirname);
    fs.mkdirSync(dirname);
  }
}