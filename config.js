module.exports = {
  dev: true,
  host: "127.0.0.1",
  port: 3075,
  log: {
    prefix: "tcs",
    info: true,
    warning: true,
    verbose: true
  },
  db: {
    name: "circuits"
  }
}