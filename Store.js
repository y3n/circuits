module.exports = class Store {
  constructor(n) {
    this.n = n;
    this.log = n.log;
    this.config = n.config;
    this.mdb = n.db;

    this.collections = {};
    this.ops = {};
    this.closed = false;
  }

  close() {
    this.closed = true;
  }

  async getSnapshot(cName, docName, callback) {
    this.log.verbose("getSnapshot");
    let snapshot = await this.mdb.collection("ldb.snapshots").findOne({ cName: cName, docName: docName }, { projection: { _id: 0 } }) || undefined;
    if(snapshot) snapshot.data = JSON.parse(snapshot.data);
    callback(null, snapshot);
  }

  async writeSnapshot(cName, docName, snapshot, callback) {
    this.log.verbose("writeSnapshot");
    snapshot.data = JSON.stringify(snapshot.data);
    let snapshotClone = Object.assign({ cName: cName, docName: docName }, snapshot);
    await this.mdb.collection("ldb.snapshots").updateOne(
      { cName: cName, docName: docName },
      { $set: snapshotClone },
      { upsert: true }
    );
    callback();
  }

  async writeOp(cName, docName, opData, callback) {
    this.log.verbose("writeOp");
    if(opData.create) opData.create = JSON.stringify(opData.create);
    if(opData.op) opData.op = JSON.stringify(opData.op);
    let opDataClone = Object.assign({
      cName: cName,
      docName: docName,
      insertDate: new Date()
    }, opData);
    await this.mdb.collection("ldb.ops").insertOne(opDataClone, { upsert: true });
    callback();
  }

  async getVersion(cName, docName, callback) {
    this.log.verbose("getVersion");
    let lastOp = await this.mdb.collection("ldb.ops").findOne(
      { cName, docName },
      { sort: { insertDate: -1 } });
    let version = lastOp.v;

    callback(null, version);
  }

  async getOps(cName, docName, start, end, callback) {
    this.log.verbose("getOps");
    let req = this.mdb.collection("ldb.ops").find(
      {
        cName, docName,
        v: {
          $gte: start,
          $lt: end || undefined
        }
      });
    
    const ops = await req.project({ _id: 0 }).toArray();

    ops.forEach((op) => {
      if(op.create) op.create = JSON.parse(op.create);
      if(op.op) op.op = JSON.parse(op.op);
    })

    callback(null, ops);
  }
}