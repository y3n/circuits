const querystring = require('querystring');
const fs = require('fs');
const fsP = require('fs').promises;

const Duplex = require('stream').Duplex;
const share = require('share');
const livedb = require('livedb');
const Store = require('./Store');
const browserChannel = require('browserchannel').server;

module.exports = class Channel {
  constructor(n, server) {
    this.n = n;

    this.config = n.config;
    this.log = n.log;
    this.server = server;
    this.db = n.db;
  }

  init() {
    this.backend = livedb.client(new Store(this.n));
    this.share = share.server.createClient({backend: this.backend});

    this.channel = browserChannel((client) => {
      const stream = new Duplex({ objectMode: true });

      stream._read = () => { };
      stream._write = (chunk, encoding, callback) => {
        if (client.state !== 'closed') {
          client.send(chunk);
        }
        callback();
      };

      client.on('message', (data) => {
        stream.push(data);
      });

      client.on('close', (reason) => {
        stream.push(null);
        stream.emit('close');
      });

      stream.on('end', () => {
        client.close();
      });

      // Give the stream to sharejs
      return this.share.listen(stream);
    })
  }

  

  handle(req, res) {
    this.channel(req, res, () => {});
  }
}