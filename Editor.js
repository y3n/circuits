const fsP = require("fs").promises;
const querystring = require("querystring");

module.exports = class Editor {
  constructor(n, server) {
    this.n = n;
    this.config = n.config;
    this.log = n.log;
    this.server = server;
    this.db = n.db;
  }

  init() {

  }

  async handle(req, res, circuitID) {
    let circuit = await this.db.collection("circuits").findOne({ id: circuitID });
    if(!circuit) {
      res.writeHead(404);
      res.end();
      return;
    }

    let content = await this.generate(req.user, circuit);
    res.setHeader("content-type", "text/html; charset=utf-8");
    res.setHeader("content-length", Buffer.byteLength(content, 'utf8'));
    res.end(content);
  }

  async generate(user, circuit) {
    let template = await fsP.readFile(`client/editel.html`, { encoding: "utf8" });
    //console.log(template);
    template = template.replace(/{CIRCUITNAME}/g, circuit.name);
    template = template.replace("{SESSION}", "0123");
    template = template.replace("{CIRCUITID}", circuit.id);
    template = template.replace("{USERID}", user.id);

    return template;
  }

  async patch(req, res, circuitID) {
    if(req.user == null) {
      res.writeHead(403);
      res.end();
      return;
    }
    const body = JSON.parse(await this.server.getBody(req));

    await this.db.collection("circuits").updateOne({ id: circuitID }, { $set: { name: body.description }});

    res.setHeader("content-type", "application/json; charset=utf-8");
    res.end(JSON.stringify({
      id: circuitID,
      description: body.description
    }));
  }

  async create(req, res) {
    if(req.user == null) {
      res.writeHead(403);
      res.end();
      return;
    }
    let id = await this.createCircuit("Circuit 1", req.user);
    res.setHeader("location", `/c/${id}`);
    res.writeHead(302);
    res.end();
  }

  async createCircuit(name, user) {
    let id = this.n.randomString.create();
    await this.db.collection("circuits").insertOne({
      id: id,
      name: name,
      user: user.id,
      creationDate: new Date(),
      lastModified: new Date()
    })
    return id;
  }

  async handleCircuits(req, res) {
    if(req.user == null) {
      res.writeHead(403);
      res.end();
      return;
    }
    let circuits = await this.db.collection("circuits").find({
      user: req.user.id,
    }).project({ _id: 0 }).toArray();

    const resBody = JSON.stringify(circuits);
    res.setHeader("content-type", "application/json");
    res.setHeader("content-length", Buffer.byteLength(resBody, 'utf8'));
    res.end(resBody);
  }

  async handleThumbUpload(req, res) {
    if(req.user == null) {
      res.writeHead(403);
      res.end();
      return;
    }
    let body = await this.server.getBody(req);
    body = body.toString();
    const form = querystring.parse(body);
    const data = Buffer.from(form.imageBody.substring(22), 'base64');
    await this.db.collection("circuits.thumbs").updateOne({
      circuitID: form.id,
    }, {
      $set: {
        length: data.byteLength,
        data
      }
    }, { upsert: true });
    const resBody = `{"result":"Success"}`;
    res.setHeader("content-type", "application/json");
    res.setHeader("content-length", Buffer.byteLength(resBody, 'utf8'));
    res.end(resBody);
  }

  async handleThumb(req, res, circuitID) {
    const image = await this.db.collection("circuits.thumbs").findOne({ circuitID });
    if(!image) {
      res.writeHead(404);
      res.end();
      return;
    }

    res.setHeader("content-type", "image/png");
    res.setHeader("content-length", image.data.buffer.byteLength);
    res.end(image.data.buffer);
  }

  async import(req, res) {
    if(req.user == null) {
      res.writeHead(403);
      res.end();
      return;
    }
    let body = JSON.parse(await this.server.getBody(req));

    const id = await this.createCircuit("Imported circuit", req.user);

    await this.db.collection("ldb.snapshots").insertOne({
      cName: "circuits",
      docName: id,
      data: body.circuit,
      m: {
        mtime: new Date(),
        ctime: new Date()
      },
      type: "http://sharejs.org/types/JSONv0",
      v: 0
    })

    const resBody = `{"result":"Success"}`;
    res.setHeader("content-type", "application/json");
    res.setHeader("content-length", Buffer.byteLength(resBody, 'utf8'));
    res.end(resBody);
  }
}