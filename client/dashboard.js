class Dashboard {
  constructor(config) {
    this.config = config;
    this.createButton = document.querySelector("#create");
    this.nextButton = document.querySelector("#next");
    this.usernameInput = document.querySelector("#usernameInput");
    this.emailInput = document.querySelector("#emailInput");
    this.circuitsList = document.querySelector("#circuitsList");
    this.accountForm = document.querySelector("#account");
    this.buttons = document.querySelector("#buttons");
    this.importFromFile = document.querySelector("#importFromFile");
    this.importFromURL = document.querySelector("#importFromURL");
    this.import = document.querySelector("#import");
    this.tcImporter = document.querySelector("#tcImporter");
    this.return = document.querySelector("#return");
  }

  run() {
    this.bind();
    this.checkUser();
  }

  bind() {
    this.createButton.addEventListener("click", () => {
      document.location = "/create";
    });

    this.nextButton.addEventListener("click", () => {
      this.createAccount();
    })

    this.importFromURL.addEventListener("click", () => {
      this.circuitsList.style.display = "none";
      this.tcImporter.style.display = "block";
    })

    this.return.addEventListener("click", () => {
      this.circuitsList.style.display = "block";
      this.tcImporter.style.display = "none";
    })

    this.importFromFile.addEventListener("click", () => {
      this.importFile();
    })
  }

  importFile() {
    if(!this.fileInput) {
      this.fileInput = document.createElement('input');
      this.fileInput.type = 'file';
      this.fileInput.accept = 'application/json';

      this.fileInput.addEventListener("change", (e) => {
        const reader = new FileReader();
        reader.readAsText(this.fileInput.files[0], 'UTF-8');
  
        // here we tell the reader what to do when it's done reading...
        reader.onload = async readerEvent => {
          const content = readerEvent.target.result; // this is the content!
          const res = await this.req("POST", "/import", { circuit: content });
          this.getCircuits();
        }
      })
    }
    
    this.fileInput.click();
  }

  async checkUser() {
    this.key = localStorage.getItem("userKey");
    if (this.key) {
      const account = await this.req("POST", "/account", { key: this.key });
      if (!account) return false;
      this.username = localStorage.getItem("username");
      this.userEmail = localStorage.getItem("userEmail");
      this.circuitsList.style.display = "block";
      this.buttons.style.display = "block";
      this.accountForm.style.display = "none";
      this.getCircuits();
      return true;
    }
    return false;
  }

  async createAccount() {
    let username = this.usernameInput.value;
    let email = this.emailInput.value;
    if (email.length < 3 || username.length < 1) {
      return;
    }
    const account = await this.req("POST", "/account", { username, email });
    localStorage.setItem("userKey", account.key);
    localStorage.setItem("username", account.username);
    localStorage.setItem("userEmail", account.email);
    this.checkUser();
  }

  async getCircuits() {
    const circuits = await this.req("GET", "/circuits");

    circuits.forEach((circuit) => {
      circuit.creationDate = new Date(circuit.creationDate);
      circuit.lastModified = new Date(circuit.lastModified);
    })

    this.displayCircuits(circuits);
  }

  displayCircuits(circuits) {
    this.circuitsList.innerHTML = "";

    circuits.forEach((circuit, i) => {
      let el = document.createElement("div");
      el.classList.add("circuitItem");

      let elPicture = document.createElement("div");
      elPicture.classList.add("circuitItemPic");
      elPicture.style.backgroundImage = `url(/thumbs/${circuit.id}.png)`;
      el.appendChild(elPicture);

      let elMeta = document.createElement("div");
      elMeta.classList.add("circuitItemMeta");

      let elTitle = document.createElement("p");
      elTitle.classList.add("circuitItemTitle");
      elTitle.innerText = circuit.name;
      elMeta.appendChild(elTitle);

      let elDate = document.createElement("p");
      elDate.classList.add("circuitItemDate");
      elDate.innerText = circuit.lastModified.toString();
      elMeta.appendChild(elDate);

      el.appendChild(elMeta);
      el.addEventListener("click", () => {
        document.location = `/c/${circuit.id}`;
      })
      this.circuitsList.appendChild(el);
    })
  }

  async req(method, url, data) {
    // fetch api
    const rawResponse = await fetch(url, {
      method: method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: data ? JSON.stringify(data) : undefined
    });
    return await rawResponse.json();
  }

  genID() {
    let size = 21;
    let id = ''
    let bytes = crypto.getRandomValues(new Uint8Array(size))

    // A compact alternative for `for (var i = 0; i < step; i++)`.
    while (size--) {
      // It is incorrect to use bytes exceeding the alphabet size.
      // The following mask reduces the random byte in the 0-255 value
      // range to the 0-63 value range. Therefore, adding hacks, such
      // as empty string fallback or magic numbers, is unneccessary because
      // the bitmask trims bytes down to the alphabet size.
      let byte = bytes[size] & 63
      if (byte < 36) {
        // `0-9a-z`
        id += byte.toString(36)
      } else if (byte < 62) {
        // `A-Z`
        id += (byte - 26).toString(36).toUpperCase()
      } else if (byte < 63) {
        id += '_'
      } else {
        id += '-'
      }
    }
    return id
  }
}

const dashboardInst = new Dashboard({});

document.addEventListener("DOMContentLoaded", () => {
  dashboardInst.run();
})