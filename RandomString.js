const crypto = require('crypto');

module.exports = class ID {
  constructor() {}

  // NANOID based
  create(size) {
    let random = crypto.randomBytes;
    let url = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

    size = size || 24
    let id = ''
    let bytes = random(size)
    while (0 < size--) {
      id += url[bytes[size] & 61]
    }
    return id
  }
}
